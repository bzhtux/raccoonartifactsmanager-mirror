import os
import sys

# Add current dir to sys.path
try:
    sys.path.append(os.path.abspath(os.curdir))
    from raccoon import app
    from raccoon import raccoon_logger
    from raccoon.routes import Routing
except Exception as e:
    # print "run.except: %s" % e
    sys.exit(1)


# test logger
logger = raccoon_logger.get_logger()
logger.info('Start app %s : %s' % (app.config['APP_NAME'], __name__))

# Let's preprare routes
routes = Routing()

# Let's run this app
app.run()
