# Contributing

## Git conventions

### Commit messages

Commit messages are formatted following some simple rules. As a picture is better than words, this is a fake sample git message:

```
feat(login): Add a password recovery process

send an email with a one time reset url
display a form to define a new password

Close #1234
```

**feat** is a type of commit message (feat for feature). Available types are :

* feat
* fix
* refact
* conf

**login** is the scope of the commit. Here it is login scope.

**title** is `'Add a password recovery process'`. Title should always refer to application functionality never to git or code.

**`Close #1234`** is the footer, it refers to an issue in my backlog.

This is a template of commit message:

```
<type>(<scope>): <title>
<BLANK LINE>
<short description>
<BLANK LINE>
<footer>
```

Using such format is very powerfull to auto generate `CHANGELOG`. 

### Git rebase vs merge

When you work on your feature branch we prefer you to use rebase to keep a clean history of your work.
When you create a `Merge Request` your branch will be merged onto `master`, not rebase to keep history of feature apparition in master branch.

Git rebase usefull command:

```
$ git rebase -i master
```