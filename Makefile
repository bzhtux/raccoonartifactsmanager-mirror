.PHONY: test clean pep8 coverage test run

help:
	@echo "Available actions:"
	@echo " - clean:		delete all pyc files"
	@echo " - pep8:		perform pep8 checks on all python files"
	@echo " - coverage:		perform coverage run and report one all python files"
	@echo " - test:		run unit tests"
	@echo " - run:			run this app"

clean:
	@find . -type f -name "*.pyc" -delete

pep8:
	@find . -type f -name "*.py" -exec pep8 --statistics --count -v --ignore=E501,E124,E128 --exclude='./docs/*' {} \;

coverage:
	@echo run.py
	@coverage xml -o reports/coverage/run.xml run.py
	@for i in `find tests raccoon -type f -name "*.py"`; do \
		echo $$i; \
		report_file=`basename -s .py $$i`; \
		coverage xml -o reports/coverage/$${report_file}.xml $$i; \
	done

test: clean
	@python tests.py

run: clean
	@python run.py
