# Raccoon Artifacts Manager

Raccoon Artifacts Manager is a Python application built upon Python Flask, that provide an API to store several artifacts' version such as a git repository does.

## Installation

TODO

## Quickstart

TODO

## User guide

TODO

## Development

### Framework

**Python Flask** is used to deliver this artifactory app because it is lighter than other heavy frameworks (e.g *Django*).

#### Plugins

* **Flask-Uploads**: let users to upload artifacts using Raccoon AM API.
* **Celery**: let's do some async tasks

### App Architecture

Raccoon Artifacts Manager only provide a Rest API.

### Async tasks

As we need `non blocking` tasks for some methods, we use **Python Celery**.

# Contributing

[How to contribute](CONTRIBUTING.md)