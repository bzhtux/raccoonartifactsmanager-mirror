from flask import Flask, jsonify, request, send_from_directory, current_app, abort
from flask_uploads import UploadSet, configure_uploads, ARCHIVES
from raccoon.logger import Logger

app = Flask(__name__, instance_relative_config=True)
app.config.from_pyfile('flask.cfg')

raccoon_logger = Logger(app.config)
