import logging
from logging.handlers import RotatingFileHandler


class Logger():

    def __init__(self, config):
        # create logger objects
        self.raccoon_logger = logging.getLogger(__name__)
        # set log level to DEBUG (to catch all msgs) to logger objects
        self.raccoon_logger.setLevel(logging.DEBUG)
        # create a formatter for logger objects
        formatter = logging.Formatter('%(asctime)s - [%(levelname)s] - %(message)s')
        # create file handlers for each logger
        # defaults are :
        # 1 backup file
        # max size is 1Mo
        fh_raccoon = RotatingFileHandler(config['LOG_FILE'], 'a', 1000000, 1)
        # set log level to file handlers - set it to DEBUG
        fh_raccoon.setLevel(logging.DEBUG)
        # add formatter to file handlers
        fh_raccoon.setFormatter(formatter)
        # set file_handlers to logger objects
        self.raccoon_logger.addHandler(fh_raccoon)

    def get_logger(self):
        return self.raccoon_logger
