from flask import jsonify, abort, request
from raccoon import app


class Routing():

    @app.route('/', methods=['GET'])
    def index():
        error_message = {"msg": "There is nothing here, please visit documentation",
                         "doc-link": "http://raccoonartifactmanager.readthedocs.io/en/latest",
                         "scm-link": "https://github.com/bzhtux/RaccoonArtifactsManager",
                         "return": "Error",
                         "status_code": 404
        }
        return jsonify({"Message": error_message})

    @app.route('/api/{api_version}/organization'.format(api_version=app.config['API_VERSION']), methods=['GET'])
    def list_organizations():
        msg = {"msg": "/api/{api_version}/organization".format(api_version=app.config['API_VERSION'])}
        return jsonify({"Message": msg})
